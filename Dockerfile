FROM python:3.9

WORKDIR /var/www/html/

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y

RUN pip install discord2 py-cord emoji Pillow asyncpraw opencv-python validators beautifulsoup4

CMD ["python", "/var/www/html/main.py"]