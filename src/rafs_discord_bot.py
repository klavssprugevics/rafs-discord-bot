import os
import sys
import discord
import logging
from datetime import datetime

class RafsDiscordBot:

    def __init__(self, secret):
        self.bot = discord.Bot(intents=self.setup_intents())
        self.secret = secret
        self.setup_logging()

        bot = self.bot
        @bot.event
        async def on_ready():
            print(f"{self.bot.user} succesfully logged in!")
            logging.info(f"{self.bot.user} succesfully logged in!")

    def start_bot(self):
        self.bot.run(self.secret)

    def add_cog(self, cog):
        self.bot.add_cog(cog)

    def setup_logging(self):

        if not os.path.exists("./logs/"):
            os.makedirs("./logs/")
        
        file_handler = logging.FileHandler(filename='./logs/' + datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '.log',
        encoding='utf-8')
        stdout_handler = logging.StreamHandler(stream=sys.stdout)
        handlers = [file_handler, stdout_handler]

        logging.basicConfig(
            level=logging.INFO, 
            format='%(asctime)s %(levelname)-8s %(message)s',
            handlers=handlers
        )

    def setup_intents(self):
        intents = discord.Intents.default()
        intents.message_content = True
        return intents
