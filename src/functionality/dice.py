import discord
import numpy as np
import emoji
from src.utils.dice_utils import roll_dice, format_output

class Dice(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="dice", description="Roll a dice!")
    async def dice(self, ctx):
        await ctx.respond(format_output(roll_dice(1, 6)))

    @discord.slash_command(name="eightdice", description="I have 8 dice, which number will be most frequent?")
    async def eight_dice(self, ctx, guess:int=None):
        
        res = [0, 0, 0, 0, 0, 0]
        for i in range(0, 8):
            res[roll_dice(1, 6) - 1] += 1
        
        out = ""
        frequents = []
        largest_count = np.max(res)
        for i in range(1, 7):
            if largest_count == res[i - 1]:
                frequents.append(i)
            
            out += f"{format_output(i)}: {res[i - 1]} time(s)\n"
        

        if len(frequents) > 1:
            out += f"The most frequent numbers were: "
            for val in frequents:
                out += f"{format_output(val)} "
            out += "\n"
        else:
            out += f"The most frequent number was: {format_output(frequents[0])}\n"

        if guess is not None:
            if guess in frequents:
                out += emoji.emojize(":white_check_mark: You won!")
            else:
                out += emoji.emojize(":x: You lost!")

        await ctx.respond(out)

    @discord.slash_command(name="choose", description="Don't make me choose :( Seperate options with a comma")
    async def choose(self, ctx, options:str):
        options_list = options.split(",")

        if len(options_list) < 2:
            await ctx.respond("Nothing to choose from!")
        else:
            await ctx.respond(f"Go with: {options_list[roll_dice(0, len(options_list) - 1)]}")
