import discord
from src.utils.katrinas_fakti_utils import get_katrinas_fact

class KatrinasFakti(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="katrinasfakts", description="The best kind of facts!")
    async def katrinas_fakts(self, ctx):
        fact = get_katrinas_fact()
        if fact is None:
            await ctx.respond("Error fetching Katrīnas facts!")
        else:
            await ctx.respond(fact)
