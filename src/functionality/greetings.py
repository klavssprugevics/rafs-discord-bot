import discord
import logging

class Greetings(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="hello", description="Introduce yourself!")
    async def hello(self, ctx, name: str = None):
        name = name or ctx.author.name
        await ctx.respond(f"Hello {name}!")
        logging.info(f"{ctx.author} says hello")
