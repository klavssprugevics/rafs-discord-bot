import discord
import logging
import PIL
from src.utils.radial_blur import RadialBlur
from src.utils.message_utils import MessageUtils
from src.utils.image_utils import is_filename_valid_extension

class ImageManipulation(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="radialblur", description="Radial blur an image!")
    async def radial_blur(self, ctx, image_url : str = None):

        # If no image_url is provided, check if recent messages in the channel have an image
        if image_url is None:
            mu = MessageUtils(self.bot) 
            await mu.find_last_image_by_user(ctx.channel_id, limit=3, url_allow=True)
            image_url = mu.last_image 

        # If no image found - error
        if image_url is None:
            logging.warning(f"{ctx.author} tried to blur an image with no url")
            await ctx.respond("No image provided")
            return
            
        if not is_filename_valid_extension(image_url):
            logging.warning(f"Invalid image url provided: {image_url}")
            await ctx.respond(content=f"Invalid image extension!")
            return

        try:
            rb = RadialBlur(image_url)
        except PIL.UnidentifiedImageError:
            logging.warning(f"Error processing image: {image_url}")
            await ctx.respond(content=f"Error processing image!")
            
        try:
            blurred_img = open(rb.get_blurred_image_path(), "rb")
        except OSError:
            logging.warning(f"Could not open image: {blurred_img}")
            await ctx.respond(content=f"An error occured!")
        else:
            with blurred_img:
                await ctx.respond(file=discord.File(blurred_img))
            logging.info(f"{ctx.author.name} blurred an image")
