import discord
import logging
from src.utils.weather_utils import WeatherAPI

class Weather(discord.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.weatherAPI = WeatherAPI()

    @discord.slash_command(name="weather", description="Get the weather for a city")
    async def weather_for_city(self, ctx, city: str):

        weather = self.weatherAPI.get_weather_for_city(city)
        if weather is None:
            await ctx.respond("Error fetching weather information")
        else:
            logging.info(f"{ctx.author.name} called for weather")
            await ctx.respond(weather)

    @discord.slash_command(name="forecast", description="Get the 5-day weather forecast for a city")
    async def forecast_for_city(self, ctx, city: str):

        forecast = self.weatherAPI.get_forecast_for_city(city)
        if forecast is None:
            await ctx.respond("Error fetching weather information")
        else:
            logging.info(f"{ctx.author.name} called for weather")
            await ctx.respond(forecast)
