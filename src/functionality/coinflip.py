import discord
import logging
import random

class Coinflip(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="coinflip", description="Flip a coin!")
    async def coinflip(self, ctx):
        
        if random.random() < 0.5:
            filename = "./resources/coinflip/heads.png"
            logging.info(f"{ctx.author} flipped Heads")

            try:
                img = open(filename, "rb")
            except OSError:
                logging.warning(f"Could not open image: {filename}")
                await ctx.respond(content=f"Heads!")
            else:
                with img:
                    await ctx.respond(content=f"Heads!", file=discord.File(img))
        else:
            filename = "./resources/coinflip/tails.png"
            logging.info(f"{ctx.author} flipped Tails")

            try:
                img = open(filename, "rb")
            except OSError:
                logging.warning(f"Could not open image: {filename}")
                await ctx.respond(content=f"Tails!")
            else:
                with img:
                    await ctx.respond(content=f"Tails!", file=discord.File(img))
