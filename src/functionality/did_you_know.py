import discord
import logging
from src.utils.random_fact import get_random_fact, get_todays_fact

class DidYouKnow(discord.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(name="didyouknow", description="Random fact")
    async def did_you_know(self, ctx):
        fact = get_random_fact()
        if fact is None:
            await ctx.respond("Error fetching your fact")
        else:
            logging.info(f"{ctx.author.name} called for a random fact")
            await ctx.respond(fact)

    @discord.slash_command(name="fotd", description="Fact of the day")
    async def fact_of_the_day(self, ctx):
        fact = get_todays_fact()
        if fact is None:
            await ctx.respond("Error fetching your fact")
        else:
            logging.info(f"{ctx.author.name} called for today's fact")
            await ctx.respond(fact)
