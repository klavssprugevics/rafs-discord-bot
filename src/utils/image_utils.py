def is_filename_valid_extension(filename):
    pic_ext = ['.jpg','.png','.jpeg']

    valid_extension = False
    for ext in pic_ext:
        if filename.endswith(ext):
            valid_extension = True
            break
    
    return valid_extension
