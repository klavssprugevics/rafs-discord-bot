import logging

def get_katrinas_fact():

    filename = './resources/katrinas_fakti/fakti1.txt'
    fact = None

    try:
        file = open(filename, 'r+')
    except OSError:
        logging.warning(f"Error opening katrinas facts file: {filename}")
    else:
        with file:
            lines = file.readlines()
            last_line = lines[-1]
            lines = lines[:-1]

            # Prepend last line
            file.seek(0, 0)
            file.truncate()
            file.write(last_line + ''.join(lines))

            fact = last_line
    
    return fact
