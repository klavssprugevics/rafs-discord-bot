import json
import emoji
import requests
import logging
import datetime
from src.utils.json_dictionary import JsonDictionary

class WeatherAPI():
    def __init__(self):
        self.secret = JsonDictionary("./resources/secrets.json")["OpenWeatherAPIKey"]

    def get_weather_for_city(self, city):
        try:
            response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={self.secret}&units=metric")
        except requests.exceptions.ConnectionError:
            logging.warning(f"Error connecting to weather API")
            return None
        else:
            if response.status_code == requests.codes.ok:
                weather = json.loads(response.text)
                wi = WeatherInfo(weather["name"], weather["weather"][0]["main"],
                weather["weather"][0]["description"], weather["main"]["temp"],
                weather["main"]["feels_like"])
                return wi.format_info()
            else:
                return None

    def get_forecast_for_city(self, city):
        try:
            response = requests.get(f"https://api.openweathermap.org/data/2.5/forecast?q={city}&appid={self.secret}&units=metric")
        except requests.exceptions.ConnectionError:
            logging.warning(f"Error connecting to weather API")
            return None
        else:
            if response.status_code == requests.codes.ok:
                weather = json.loads(response.text)
                forecastInfo = ForecastInfo(self.parse_forecast(weather))
                return forecastInfo.format_forecast()
            else:
                return None

    def parse_forecast(self, forecast):
        date_list = []
        wi_list = []
        for i in range(0, 40):
            dt = datetime.datetime.fromtimestamp(forecast["list"][i]["dt"])
            if dt.time().hour == 12:
                curr = forecast["list"][i]
                wi_list.append(WeatherInfo(forecast["city"]["name"], curr["weather"][0]["main"], 
                curr["weather"][0]["description"], curr["main"]["temp"],
                curr["main"]["feels_like"]))
                date_list.append(dt.date())

        return wi_list, date_list

class WeatherInfo:
    def __init__(self, location, status, status_desc, temperature_actual, temperature_feels_like):
        self.location = location
        self.status = status
        self.status_desc = status_desc
        self.temperature_actual = str(temperature_actual)
        self.temperature_feels_like = str(temperature_feels_like)

    def format_info(self):
        icon = ""

        if self.status == "Clouds":
            icon = emoji.emojize(":cloud:")
        elif self.status == "Thunderstorm":
            icon = emoji.emojize(":cloud_with_lightning_and_rain:")
        elif self.status == "Drizzle":
            icon = emoji.emojize(":cloud_with_rain:")
        elif self.status == "Rain":
            icon = emoji.emojize(":cloud_with_rain:")
        elif self.status == "Snow":
            icon = emoji.emojize(":cloud_with_snow:")
        elif self.status == "Clear":
            icon = emoji.emojize(":sun_behind_small_cloud:")
        elif self.status == "Atmosphere":
            icon = emoji.emojize(":milky_way:")

        info = icon
        info += "  "
        info += self.location
        info += ": "
        info += self.status
        info += f" ({self.status_desc})"
        info += f" | Temperature: {self.temperature_actual}° (feels like {self.temperature_feels_like}°)"
        return info

class ForecastInfo:
    def __init__(self, info):
        self.wi_list = info[0]
        self.date_list = info[1]


    def format_forecast(self):
        summary = ""

        for wi, date in zip(self.wi_list, self.date_list):
            day = str(date.day)
            month = str(date.month)
            if len(day) < 2:
                day = "0" + day
            if len(month) < 2:
                month = "0" + month
                
            summary += f"{day}.{month}.{date.year}. | "
            summary += wi.format_info()
            summary += "\n"

        return summary
