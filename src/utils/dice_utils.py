import emoji
import random

def roll_dice(min, max):
    return random.randint(min, max)

def format_output(roll):
    if roll == 1:
        return emoji.emojize(":one:")
    elif roll == 2:
        return emoji.emojize(":two:")
    elif roll == 3:
        return emoji.emojize(":three:")
    elif roll == 4:
        return emoji.emojize(":four:")
    elif roll == 5:
        return emoji.emojize(":five:")
    elif roll == 6:
        return emoji.emojize(":six:")
    else:
        return roll