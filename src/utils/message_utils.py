import validators

class MessageUtils:

    def __init__(self, bot):
        self.bot = bot

    async def find_last_image_by_user(self, channel_id, limit=3, url_allow=True):
        url = None
        channel = self.bot.get_channel(channel_id)
        async for msg in channel.history(limit=limit):
            if len(msg.attachments) > 0:
                self.last_image = msg.attachments[0].url
                break
            elif validators.url(msg.content) and url_allow:
                self.last_image = msg.content
                break
