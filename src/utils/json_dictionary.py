import json

class JsonDictionary:

    def __init__(self, json_filename):
        self.dictionary = self.open(json_filename)
    
    def __getitem__(self, key):
        try:
            return self.dictionary[key]
        except KeyError:
            raise KeyError(f"No entry with key: {key}")

    def __setitem__(self, key, value):
        
        if value is not None:
            self.dictionary[key] = value
        else:
            self.dictionary.pop(key)

    def open(self, filename):
        try:
            file = open(filename)
        except OSError:
            raise OSError(f"Could not open file: {filename}")
        else:
            with file:
                return json.load(file)

    def save(self, filename):
        with open(filename, 'w') as file:
            json.dump(self.dictionary, file)
