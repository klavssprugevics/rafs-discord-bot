import json
import requests
import logging

def get_random_fact():
    try:
        response = requests.get("https://uselessfacts.jsph.pl/random.txt?language=en")
    except requests.exceptions.ConnectionError:
        logging.warning(f"Error connecting to facts API")
        return None
    else:
        if response.status_code == requests.codes.ok:
            response_json = json.loads(response.text)
            return response_json["text"]
        else:
            return None
        
def get_todays_fact():
    try:
        response = requests.get("https://uselessfacts.jsph.pl/today.txt?language=en")
    except ConnectionError:
        logging.warning(f"Error connecting to facts API")
        return None
    else:
        if response.status_code == requests.codes.ok:
            response_json = json.loads(response.text)
            return response_json["text"]
        else:
            return None
