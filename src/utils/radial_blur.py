import cv2
import requests
import numpy as np
from PIL import Image

class RadialBlur:

    def __init__(self, image_url):
        self.file_path = "./resources/radial_blur/"
        self.img = self.load_image(image_url)
        self.blurred_image = self.radial_blur()

    def load_image(self, image_url):
        img = Image.open(requests.get(image_url, stream=True).raw)
        img.save(self.file_path + "temp.png")
        return img

    def radial_blur(self):
        img =  np.asarray(self.img)
        w, h = img.shape[:2]

        center_x = w / 2
        center_y = h / 2
        blur = 0.02
        iterations = 7

        growMapx = np.tile(np.arange(h) + ((np.arange(h) - center_x)*blur), (w, 1)).astype(np.float32)
        shrinkMapx = np.tile(np.arange(h) - ((np.arange(h) - center_x)*blur), (w, 1)).astype(np.float32)
        growMapy = np.tile(np.arange(w) + ((np.arange(w) - center_y)*blur), (h, 1)).transpose().astype(np.float32)
        shrinkMapy = np.tile(np.arange(w) - ((np.arange(w) - center_y)*blur), (h, 1)).transpose().astype(np.float32)

        for i in range(iterations):
            tmp1 = cv2.remap(img, growMapx, growMapy, cv2.INTER_LINEAR)
            tmp2 = cv2.remap(img, shrinkMapx, shrinkMapy, cv2.INTER_LINEAR)
            img = cv2.addWeighted(tmp1, 0.5, tmp2, 0.5, 0)

        blurred_img = Image.fromarray(img)
        blurred_img.save(self.file_path + "blurred.png")
        return blurred_img

    def get_blurred_image_path(self):
        return self.file_path + "blurred.png"
