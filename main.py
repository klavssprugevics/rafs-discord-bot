from src.utils.json_dictionary import JsonDictionary
from src.rafs_discord_bot import RafsDiscordBot
from src.functionality.greetings import Greetings
from src.functionality.coinflip import Coinflip
from src.functionality.image_manipulation import ImageManipulation
from src.functionality.did_you_know import DidYouKnow
from src.functionality.weather import Weather
from src.functionality.katrinas_fakti import KatrinasFakti
from src.functionality.dice import Dice

if __name__ == "__main__":

    secrets = JsonDictionary("./resources/secrets.json")
    RafsBot = RafsDiscordBot(secrets["ClientSecret"])

    # Cogs
    RafsBot.add_cog(Greetings(RafsBot.bot))
    RafsBot.add_cog(Coinflip(RafsBot.bot))
    RafsBot.add_cog(ImageManipulation(RafsBot.bot))
    RafsBot.add_cog(DidYouKnow(RafsBot.bot))
    RafsBot.add_cog(Weather(RafsBot.bot))
    RafsBot.add_cog(KatrinasFakti(RafsBot.bot))
    RafsBot.add_cog(Dice(RafsBot.bot))

    RafsBot.start_bot()
