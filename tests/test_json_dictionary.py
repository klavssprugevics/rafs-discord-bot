from src.utils.json_dictionary import JsonDictionary
import unittest
import pathlib as pl


class TestJsonDictionary(unittest.TestCase):
    
    def test_file_read_correctly(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())

        json_dictionary = JsonDictionary(path)
        self.assertEqual(json_dictionary.dictionary, {'key1': 'value1','key2':'value2'})


    def test_file_doesnt_exist(self):
        path = pl.Path("./tests/resources/correct.jsony")
        self.assertFalse(path.is_file())
        
        with self.assertRaises(OSError):
            json_dictionary = JsonDictionary(path)


    def test_file_invalid_json(self):
        path = pl.Path("./tests/resources/incorrect.json")
        self.assertTrue(path.is_file())

        with self.assertRaises(ValueError):
            json_dictionary = JsonDictionary(path)


    def test_key_exists(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        self.assertEqual(json_dictionary["key1"], "value1")
        self.assertEqual(json_dictionary["key2"], "value2")


    def test_key_doesnt_exist(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        with self.assertRaises(KeyError):
            json_dictionary["key3"]


    def test_new_key_new_value(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        json_dictionary["key3"] = "value3"
        self.assertEqual(json_dictionary["key3"], "value3")


    def test_old_key_new_value(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        json_dictionary["key2"] = "value3"
        self.assertEqual(json_dictionary["key2"], "value3")


    def test_save_file_old(self):
        path = pl.Path("./tests/resources/correct.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        json_dictionary.save("./tests/resources/correct.json")

        self.assertTrue(path.is_file())
        json_dictionary2 = JsonDictionary(path)
        self.assertEqual(json_dictionary.dictionary, json_dictionary2.dictionary)


    def test_save_file_old(self):
        path = pl.Path("./tests/resources/correct.json")
        path2 = pl.Path("./tests/resources/correct_new.json")
        self.assertTrue(path.is_file())
        json_dictionary = JsonDictionary(path)

        json_dictionary["key2"] = None
        json_dictionary["key3"] = "value3"

        json_dictionary.save(path2)

        self.assertTrue(path2.is_file())
        json_dictionary2 = JsonDictionary(path2)

        self.assertEqual(json_dictionary.dictionary, json_dictionary2.dictionary)


    

if __name__ == '__main__':
    unittest.main(verbosity=2)